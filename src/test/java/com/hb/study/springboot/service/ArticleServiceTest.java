package com.hb.study.springboot.service;

import com.hb.study.springboot.cache.ArticleCacheRepository;
import com.hb.study.springboot.pojo.Article;
import com.hb.study.springboot.pojo.ArticleCache;
import com.hb.study.springboot.repository.ArticleDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import javax.persistence.Table;
import java.util.Optional;

/**
 * @author bo.huang
 * @since 2020/5/20 12:51 下午
 */
@SpringBootTest
class ArticleServiceTest {
    @Autowired
    private ArticleService articleService;

    @Autowired
    private ArticleCacheRepository cacheRepository;
    @Autowired
    private ArticleDao articleDao;

    @Test
    void selectPage() {
        Page<Article> articles = articleService.selectPage(1, 2);
        System.out.println("=====================");
        System.out.println(articles.getContent());
        System.out.println("=====================");
    }

    @Test
    void test1(){
        Optional<Article> byId = articleDao.findById(2L);
        Article article = byId.get();
        ArticleCache articleCache = new ArticleCache();
        BeanUtils.copyProperties(article,articleCache);
        cacheRepository.save(articleCache);
    }

    @Test
    void test2(){
        Article article = articleService.selectById(110L);
    }
}