package com.hb.study.springboot.controller;

import com.hb.study.springboot.pojo.Article;
import com.hb.study.springboot.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 文章控制层
 *
 * @author bo.huang
 * @since 2020/5/20 11:22 上午
 */
@Controller
public class ArticleController {

    @SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
    @Autowired
    private ArticleService articleService;

    /**
     * 分页查询
     *
     * @param modelMap modelMap
     * @param page page
     * @param size size
     * @return java.lang.String
     * @author bo.huang update
     * @date 2020/5/20 1:23 下午
     */
    @RequestMapping("/")
    public String index(ModelMap modelMap, @RequestParam(value = "page",required = false) Integer page,
                        @RequestParam(value = "size",required = false) Integer size) {
        if (page == null || page < 1) {
            page = 1;
        }
        if (size == null || size < 1) {
            size = 10;
        }
        Page<Article> articles = articleService.selectPage(page, size);
        //内容
        modelMap.put("articles", articles.getContent());
        //当前页
        modelMap.put("page", page);
        //总页数
        modelMap.put("totalPages", articles.getTotalPages());
        //总条数
        modelMap.put("totalSize", articles.getTotalElements());
        //每页显示条数
        modelMap.put("size", size);
        return "client/index";
    }
}