package com.hb.study.springboot.config;

import lombok.extern.java.Log;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;

/**
 * @author bo.huang
 * @since 2020/5/20 2:23 下午
 */
@Configuration
@Log
public class BeanConfig {

    @Bean("localeResolver")
    public LocaleResolver getLocaleResolver() {
        log.info("===========实例化LocaleResolver");
        return new LocaleResolverConfig();
    }
}