package com.hb.study.springboot.config;

import lombok.extern.java.Log;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @author bo.huang
 * @since 2020/5/20 1:43 下午
 */
@Log
public class LocaleResolverConfig implements LocaleResolver {
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        log.fine("=============语言");
        //获取语言参数
        String lg = request.getParameter("lg");
        if (lg == null || lg.length() <= 0) {
            lg = getHeadLg(request);
        }
        String[] split = lg.split("-");
        return new Locale(split[0], split[1]);
    }

    /**
     * 获取请求头中的语言头
     *
     * @param request request
     * @return java.lang.String
     * @author bo.huang update
     * @date 2020/5/20 1:47 下午
     */
    private String getHeadLg(HttpServletRequest request) {
        String header = request.getHeader("Accept-Language");
        if (header == null || header.length() <= 0) {
            return "zh-CN";
        }
        return header.split(",")[0];
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
    }


}