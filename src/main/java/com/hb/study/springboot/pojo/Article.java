package com.hb.study.springboot.pojo;

import lombok.Data;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 文章类
 *
 * @author bo.huang
 * @since 2020/5/20 11:38 上午
 */
@Data
@Table(name = "t_article")
@Entity
public class Article implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 文章标题
     */
    @Column(name = "title")
    private String title;
    /**
     * 文章具体内容
     */
    @Column(name = "content")
    private String content;
    /**
     * 发表时间
     */
    @Column(name = "created")
    private Date created;
    /**
     * 修改时间
     */
    @Column(name = "modified")
    private Date modified;
    /**
     * 文章分类
     */
    @Column(name = "categories")
    private String categories;
    /**
     * 文章标签
     */
    @Column(name = "tags")
    private String tags;
    /**
     * 是否允许评论
     */
    @Column(name = "allow_comment")
    private Integer allowComment;
    /**
     * 文章缩略图
     */
    @Column(name = "thumbnail")
    private String thumbnail;


}