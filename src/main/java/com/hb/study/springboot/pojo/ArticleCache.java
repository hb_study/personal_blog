package com.hb.study.springboot.pojo;

import lombok.Data;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 文章类
 *
 * @author bo.huang
 * @since 2020/5/20 11:38 上午
 */
@Data
@RedisHash("articleCache")
public class ArticleCache implements Serializable {

    @Id
    private Long id;
    /**
     * 文章标题
     */
    @Indexed
    private String title;
    /**
     * 文章具体内容
     */
    private String content;
    /**
     * 发表时间
     */
    private Date created;
    /**
     * 修改时间
     */
    private Date modified;
    /**
     * 文章分类
     */
    @Indexed
    private String categories;
    /**
     * 文章标签
     */
    private String tags;
    /**
     * 是否允许评论
     */
    private Integer allowComment;
    /**
     * 文章缩略图
     */
    private String thumbnail;


}