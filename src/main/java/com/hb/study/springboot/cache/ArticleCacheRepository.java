package com.hb.study.springboot.cache;

import com.hb.study.springboot.pojo.ArticleCache;
import org.springframework.data.repository.CrudRepository;

/**
 * @author bo.huang
 * @since 2020/5/20 2:32 下午
 */
public interface ArticleCacheRepository extends CrudRepository<ArticleCache, Long> {
}