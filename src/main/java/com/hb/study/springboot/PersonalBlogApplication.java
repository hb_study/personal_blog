package com.hb.study.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 个人博客启动入口
 *
 * @author bo.huang update
 * @date 2020/5/20 11:06 上午
 */
@SpringBootApplication
@EnableCaching
public class PersonalBlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonalBlogApplication.class, args);
    }

}
