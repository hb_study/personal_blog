package com.hb.study.springboot.service;

import com.hb.study.springboot.cache.ArticleCacheRepository;
import com.hb.study.springboot.pojo.Article;
import com.hb.study.springboot.repository.ArticleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author bo.huang
 * @since 2020/5/20 11:36 上午
 */
@Service
public class ArticleService {

    @SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
    @Autowired
    private ArticleDao articleDao;

    @Autowired
    private ArticleCacheRepository articleCacheRepository;

    /**
     * 分页查询
     *
     * @param page 当前页
     * @param size 每页显示条数
     * @return org.springframework.data.domain.Page<com.hb.study.springboot.pojo.Article>
     * @author bo.huang update
     * @date 2020/5/20 11:44 上午
     */
    public Page<Article> selectPage(Integer page, Integer size) {
        PageRequest pageable = PageRequest.of(page - 1, size, Sort.Direction.DESC, "id");
        return articleDao.findAll(pageable);
    }

    /**
     * 注意 @Cacheable 与事务注解有冲突,高并发下在事务未提交前,删除缓存,再去查询就是未提交前的数据
     *
     * @param id id
     * @return com.hb.study.springboot.pojo.Article
     * @author bo.huang update
     * @date 2020/5/20 3:20 下午
     */
    @Cacheable(cacheNames = "articleService", key = "#id", unless = "#result==null")
    public Article selectById(Long id) {
        Optional<Article> byId = articleDao.findById(id);
        return byId.orElse(null);
    }


}