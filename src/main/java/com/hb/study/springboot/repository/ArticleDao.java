package com.hb.study.springboot.repository;

import com.hb.study.springboot.pojo.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author bo.huang
 * @since 2020/5/20 11:36 上午
 */
public interface ArticleDao extends JpaRepository<Article, Long>, PagingAndSortingRepository<Article, Long> {

}
